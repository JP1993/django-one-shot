from django.shortcuts import render, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoForm, ItemForm


def todo_list_list(request):
    todo_list = TodoList.objects.all()
    context = {"todo_list": todo_list}
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    todo_detail = TodoList.objects.get(id=id)
    context = {"todo_detail": todo_detail}
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            # To redirect to the detail view of the model, use this:
            asdf = form.save()
            return redirect("todo_list_detail", id=asdf.id)

    else:
        form = TodoForm()

    context = {"form": form}

    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    todo_detail = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=todo_detail)
        if form.is_valid():
            asdf = form.save()
            return redirect("todo_list_detail", id=asdf.id)
    else:
        form = TodoForm(instance=todo_detail)

    context = {"form": form}

    return render(request, "todos/update.html", context)


def todo_list_delete(request, id):
    todo_delete = TodoList.objects.get(id=id)
    if request.method == "POST":
        todo_delete.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")


def item_create(request):
    if request.method == "POST":
        form = ItemForm(request.POST)
        if form.is_valid():
            asdf = form.save()
            return redirect("todo_list_detail", id=asdf.list.id)
    else:
        form = ItemForm()
    context = {
        "form": form,
    }
    return render(request, "todo_items/create.html", context)


def item_update(request, id):
    todo_update = TodoItem.objects.all()
    if request.method == "POST":
        form = ItemForm(request.POST, instance=todo_update)
        if form.is_valid():
            asdf = form.save()
            return redirect("todo_list_detail", id=asdf.list.id)
    else:
        form = ItemForm()
    context = {
        "form": form,
    }
    return render(request, "todo_items/update.html", context)
